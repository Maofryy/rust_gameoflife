extern crate rand;
extern crate term_size;

use rand::Rng;

fn neighboor_count(grid : [[u8; 400]; 100], x : isize, y : isize) -> u8 {
    // Calculating the neighboor sum of each cell
    let (mut x_top_offset, mut x_bot_offset, mut y_left_offset, mut y_right_offset) : (isize, isize, isize, isize) = (0, 0, 0, 0);
    let mut sum = 0;
    
    match x {
        0 => x_top_offset = 99,
        99 => x_bot_offset = -99,
        _ => print!(""),
    }
    match y {
        0 => y_left_offset = 399,
        399 => y_right_offset = -399,
        _ => print!(""),
    }
    //Top line of neigboor cells
    sum += grid[(x + x_top_offset - 1) as usize][(y + y_left_offset - 1) as usize];
    sum += grid[(x + x_top_offset - 1) as usize][y as usize];
    sum += grid[(x + x_top_offset - 1) as usize][(y + y_right_offset + 1) as usize];

    //Bottom line of neigboor cells
    sum += grid[(x + x_bot_offset + 1) as usize][(y + y_left_offset - 1) as usize];
    sum += grid[(x + x_bot_offset + 1) as usize][y as usize];
    sum += grid[(x + x_bot_offset + 1) as usize][(y + y_right_offset + 1) as usize];

    //Middle lines
    sum += grid[x as usize][(y + y_left_offset - 1) as usize];
    sum += grid[x as usize][(y + y_right_offset + 1) as usize];

    return sum;
}

fn neighboor_map(grid : [[u8; 400]; 100]) -> [[u8; 400]; 100]{
    // Computing a map of the number of neighboors of each cells
    let mut map : [[u8; 400]; 100] = [[0; 400]; 100];
    for i in 0..100{
        for j in 0..400{
            map[i][j] = neighboor_count(grid, i as isize, j as isize);
        }
    }
    return map;
}

fn next_grid (mut grid :[[u8; 400]; 100], map : [[u8; 400]; 100]) -> [[u8; 400]; 100]{
    for i in 0..100 {
        for j in 0..400 {
            // Computing changes (if they occur) according to the GoL rules
            if grid[i][j]==1 && (map[i][j] < 2 || map[i][j] > 3){
                grid[i][j] = 0;
            } else if grid[i][j]!=1 && map[i][j] == 3 {
                grid[i][j] = 1;
            }
        }
    }
    return grid;
}

fn display_grid (grid : [[u8; 400]; 100]) {
    // Fill a string with the entire grid
    let mut string;
    if let Some((width, height)) = term_size::dimensions() {    
        string = String::new();
        for i in 0..height {        
            if i != 0 {
                string.push('\n');
            }
            for j in 0..width {
                if grid[i][j]==1 {
                    string.push('■');
                } else {
                    string.push(' ');
                }
            }
            
        }
        //Then print in all at once at pos (1,1) in the terminal
        print!("{}[1;1H{}", 27 as char, string);
    }
}

fn main() {
    let mut rng = rand::thread_rng();
    let mut grid : [[u8; 400]; 100] = [[0; 400]; 100];
    let mut map : [[u8; 400]; 100];

    // Fill with random
    
    for i in 0..100 {
        for j in 0..400 {
            grid[i][j] = rng.gen_range(0, 2);
        }
    }
    // Looping the update functions
    loop
    {
        // Display the current grid
        display_grid(grid);
        // Generating neighboor count map
        map = neighboor_map(grid);
        // Calculate the next grid
        grid = next_grid(grid, map);
    }
}
